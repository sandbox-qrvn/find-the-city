/**
 * Init const
 * MAX_DISTANCE représente distance en km max à laquelle on peut gagner des pts
 */
 const MAX_PTS = 5000;
 const MAX_DISTANCE = 300;
 
 //Init btn
 let btnValider = document.getElementById("valider");
 let btnContinuer = document.getElementById("continuer");
 let consigne = document.getElementById("consigne");
 let resultat = document.getElementById("resultat");
 
 btnValider.style.display = "none";
 btnContinuer.style.display = "none";
 resultat.style.display = "none";
 
 
 //Villes jouable
 var villes = {
     "Paris": {"lat": 48.856809, "lon": 2.347126, "name": "Paris"},
     "Brest": {"lat": 48.400002, "lon": -4.48333, "name": "Brest"},
     "Rennes": {"lat": 48.083328, "lon": -1.68333, "name": "Rennes"},
     "Lyon": {"lat": 45.764043, "lon": 4.835659, "name": "Lyon"},
     "Bordeaux": {"lat": 44.837789, "lon": -0.57918, "name": "Bordeaux"},
     "Marseille": {"lat": 43.296482, "lon": 5.36978, "name": "Marseille"},
     "Nancy": {"lat": 48.692054, "lon": 6.184417, "name": "Nancy"},
     "Dijon": {"lat": 47.322047, "lon": 5.04148, "name": "Dijon"},
 };
 
 //Génère point aléatoire
 var createRandomMarker = function (){
     var randomMarker;
     var rand = Math.floor(Math.random() * 8), i=0;
     for (var ville in villes){
         if(rand == i){
             randomMarker = L.marker([villes[ville].lat, villes[ville].lon]).bindTooltip(villes[ville].name,{
                 permanent: true
             });
             return randomMarker;
         }
         i++;
     }
 };
 
 //Init map et début game
 var map = L.map('map');
 map.on('load',function(e){});
 map.setView([46.56832, 2.504883], 6);
 
 L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png',{
     attribution: 'données openstreetmap',
     minZoom: 6,
     maxZoom: 6
 }).addTo(map);
 
 //Création du marqueur aléatoire, mis dans groupe et groupe ajouté à la carte
 var objectif = createRandomMarker();
 var randomMarkerGroup = L.layerGroup();
 objectif.addTo(randomMarkerGroup);
 
 var markers = L.layerGroup();
 var resultatGroup = L.layerGroup();
 
 //Init header html
 var question = document.getElementById("question");
 question.innerHTML = "Où est " +  objectif.getTooltip().getContent() + " ?";
 
 //Event bouton valider
 var valider = document.getElementById("valider");
 valider.addEventListener('click',function(e){
     //alert("salut" + markers.getLayers()[1].closeTooltip());
     //markers.removeFrom(map);
     randomMarkerGroup.addTo(map);
     
     coordRand = randomMarkerGroup.getLayers()[0].getLatLng();
     coordMarker = markers.getLayers()[0].getLatLng();
 
     var distance = map.distance(coordMarker, coordRand);
     distance = distance / 1000;
     distance = Math.round(distance * 100) / 100;
 
     var coord = [
         coordMarker,
         coordRand
     ];
     var line = L.polyline(coord, {color: 'red'});
     line.bindTooltip(distance + " km", {
         permanent: true
     });
 
     if(resultatGroup.getLayers().length > 0)
         resultatGroup.clearLayers();
 
     line.addTo(resultatGroup);
     resultatGroup.addTo(map);
 
     //Calcule points
     var score = MAX_PTS;
     if(distance > MAX_DISTANCE)
         score = 0;
     else{
         var pct = distance*100 / MAX_DISTANCE;
         score = score - (score * pct/100);
         score = Math.round(score);
         if(distance < 2)
             score = MAX_PTS;
     }
 
     //Afficher bouton continuer + score & desactiver bouton valider
     btnContinuer.style.display = "block";
     btnValider.style.display = "none";
     resultat.style.display = "block";
     resultat.innerHTML = "Marqueur distant de " +distance + " km. Points: " +score;
 });
 
 //Event 
 var continuer = document.getElementById("continuer");
 continuer.addEventListener('click', function(e){
     //Reset group
     randomMarkerGroup.removeFrom(map);
     markers.removeFrom(map);
     resultatGroup.removeFrom(map);
 
     //Création d'un nouveau marqueur aléatoire
     do{
         var randMarkerTmp = createRandomMarker();
     }while(randMarkerTmp.getTooltip().getContent() == objectif.getTooltip().getContent());
     objectif = randMarkerTmp;
 
     randomMarkerGroup = L.layerGroup();
     objectif.addTo(randomMarkerGroup);
 
     //Init header html
     question.innerHTML = "Où est " +  objectif.getTooltip().getContent() + " ?";
 
     //Reset interface
     btnContinuer.style.display = "none";
     resultat.style.display = "none";
     consigne.style.display = "block";
 });
 
 //Event click map
 map.on('click', function(e){
     //randomMarkerGroup.removeFrom(map);1
 
     if(getComputedStyle(btnContinuer).display != "block"){
         if(resultatGroup.getLayers().length > 0)
             resultatGroup.clearLayers();
     
         if(markers.getLayers().length > 0)
             markers.clearLayers();
         
         var marker = L.marker(e.latlng).addTo(markers);
         markers.addTo(map);
 
         btnValider.style.display = "block";
         consigne.style.display = "none";
     }
 });
 
 
 
 